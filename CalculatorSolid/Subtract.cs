﻿public class Subtract : IOperation,IHasName
{
    public double Execute(double a, double b)
    {
        return (a-b);
    }

    public string Name()
    {
        return "subract";
    }
}
