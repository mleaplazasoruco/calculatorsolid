﻿using System;

public class PostgreSQLDAO : IDatabaseAccess
{
    private IOperation _operation;
    public PostgreSQLDAO(IOperation operation)
    {
        _operation = operation;
    }
    public void Save()
    {
        Console.WriteLine($"PostgreSQL Saved Operation: {_operation.Name()}");
    }
}
