﻿public interface IOperation
{
    public double Execute(double a, double b);
    public string Name();
}
