﻿using System;

namespace CalculatorSolid
{
    class Program
    {
        public static double a { get; set; }
        public static double b { get; set; }
        static void Main(string[] args)
        {
            Console.WriteLine("I'm a Solid Calculator");
            IOperation operation;
            IDatabaseAccess dbAccess;
            Console.WriteLine("1.-add");
            Console.WriteLine("2.-subtract");
            Console.WriteLine("3.-multiply");
            Console.WriteLine("4.-divide");
            Console.WriteLine("5.-Unknown Operation");
            bool flag = true;
            while(flag)
            {
            var menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        SetValues();
                        operation = new Add();
                        dbAccess = new PostgreSQLDAO(operation);
                        print(operation.Execute(a, b));
                        dbAccess.Save();
                        break;
                    case "2":
                        SetValues();
                        operation = new Subtract();
                        dbAccess = new PostgreSQLDAO(operation);
                        print(operation.Execute(a, b));
                        dbAccess.Save();
                        break;
                    case "3":
                        SetValues();
                        operation = new Multiply();
                        dbAccess = new PostgreSQLDAO(operation);
                        print(operation.Execute(a, b));
                        dbAccess.Save();
                        break;
                    case "4":
                        SetValues();
                        operation = new Divide();
                        dbAccess = new PostgreSQLDAO(operation);
                        print(operation.Execute(a, b));
                        dbAccess.Save();
                        break;

                    default:
                        flag = false;
                        break;
                }
                Console.WriteLine("1.-add");
                Console.WriteLine("2.-subtract");
                Console.WriteLine("3.-multiply");
                Console.WriteLine("4.-divide");
            }
        }
        static void print(double result)
        {
            Console.WriteLine($"The result is:{result}");
        }
        static void SetValues()
        {
            Console.WriteLine("Set the value of the first number");
            a = double.Parse(Console.ReadLine());
            Console.WriteLine("Set the value of the second number");
            b = double.Parse(Console.ReadLine());
        }
    }
}
