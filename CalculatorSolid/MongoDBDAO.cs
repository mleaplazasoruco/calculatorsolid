﻿using System;

public class MongoDBDAO : IDatabaseAccess
{
    private IOperation _operation;
    public MongoDBDAO(IOperation operation)
    {
        _operation = operation;
    }
    public void Save()
    {
        Console.WriteLine($"MongoDB Saved Operation: {_operation.Name()}");
    }
}
