﻿public class Divide : IOperation,IHasName
{
    public double Execute(double a, double b)
    {
        return (a / b);
    }

    public string Name()
    {
        return "divide";
    }
}
