﻿public interface IHasName
{
    public string Name();
}